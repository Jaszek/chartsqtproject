#ifndef DATA_H
#define DATA_H

#include <QString>
#include <vector>

class Data
{
private:
    float start;
    float stop;
    float step;
    float null;
    void generateHorizontalAxis();
    void generateSortedVerticalData();
    std::vector<float> sortedNotNullVerticalAxis;


public:
    Data( const QString& fileName );

    float getStart() const;
    float getStop() const;
    float getStep() const;
    float getNull() const;
    int getCountOfNulls() const;
    float getOneValuePointedByPercent( const float& ) const;
    float getOneHorizontalValuePointedByPercent( const float& ) const;
    void pickNewDataSheet( const int& );
    bool isAnyNegativeValue() const;

    int currentlyPickedDataSheet;
    std::vector<float> horizontalAxis;
    std::vector< std::vector<float> > allVerticalAxis;
    std::vector<QString> chartsName;



};

#endif // DATA_H
