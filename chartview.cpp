#include "chartview.h"
#include <QDebug>
#include <QLabel>
#include <memory>
#include <QLogValueAxis>

using namespace QtCharts;

ChartView::ChartView(QtCharts::QChart *chart, const bool& isLog, const bool& isPosOn, QWidget *parent) : QChartView(chart), _chart(chart), m_coordX(0), m_coordY(0)
{
    this->isLog = isLog;
    this->isPosOn = isPosOn;

    QBrush brush{ QColor(255,255,255) };
    QPen pen{ brush, 2 };

    m_coordX = new QGraphicsSimpleTextItem(_chart);
    m_coordX->setPen( pen );
    m_coordX->setZValue(10);

    m_coordY = new QGraphicsSimpleTextItem(_chart);
    m_coordY->setPen( pen );
    m_coordY->setZValue(10);
}

void ChartView::setPositionTracing( const bool& newVal)
{
    isPosOn = newVal;
}

void ChartView::mouseMoveEvent(QMouseEvent *event)
{
    /* Setting the mouse position label on the axis from value to position */
    qreal x = isPosOn ? (event->pos()).x() : (0);
    qreal y = isPosOn ? (event->pos()).y() : (0);

    if( x == 0 && y == 0)
    {
        QGraphicsView::mouseMoveEvent(event);
        return;
    }

    qreal maxX = 0;
    qreal minX = 0;
    if(isLog)
    {
        auto axisX = dynamic_cast<QLogValueAxis*>(_chart->axisX());
        maxX = axisX->max();
        minX = axisX->min();
    }
    else
    {
        auto axisX = dynamic_cast<QValueAxis*>(_chart->axisX());
        maxX = axisX->max();
        minX = axisX->min();
    }

    auto axisY = dynamic_cast<QValueAxis*>(_chart->axisY());

    qreal xVal = _chart->mapToValue(event->pos()).x();
    qreal yVal = _chart->mapToValue(event->pos()).y();

    qreal maxY = axisY->max();
    qreal minY = axisY->min();


    if (xVal <= maxX && xVal >= minX && yVal <= maxY && yVal >= minY)
    {
        this->setCursor( Qt::CrossCursor );

        m_coordX->setPos(x + 6, y - 17);
        m_coordY->setPos(x + 6, y + 2);

        m_coordX->setText(QString("X = %1").arg(xVal, 4, 'f', 1, '0'));
        m_coordY->setText(QString("Y = %1").arg(yVal, 4, 'f', 1, '0'));
    }
    else
    {
        this->setCursor( Qt::ArrowCursor );

        m_coordX->setPos(-50, -50);
        m_coordY->setPos(-50, -50);
    }

    QGraphicsView::mouseMoveEvent(event);
}
