#ifndef CHARTVIEW_H
#define CHARTVIEW_H
#include <QChartView>
#include <QValueAxis>
#include <QChart>

class ChartView : public QtCharts::QChartView
{
    Q_OBJECT

public:
    explicit ChartView(QtCharts::QChart *chart, const bool& isLog, const bool& isPosOn, QWidget *parent = Q_NULLPTR);

    void setPositionTracing( const bool& );

protected:
    void mouseMoveEvent(QMouseEvent *event) override;

private:
    QtCharts::QChart *_chart;
    bool isLog;
    bool isPosOn;

    QGraphicsSimpleTextItem *m_coordX;
    QGraphicsSimpleTextItem *m_coordY;

};

#endif // CHARTVIEW_H
