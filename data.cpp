#include "data.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QRegularExpression>

Data::Data( const QString& fileName ) : start(0), stop(0), step(0), null(0)
{
   if(fileName.isEmpty())
   {
       return;
   }

   QFile file(fileName);


   if( !file.open(QIODevice::ReadOnly | QIODevice::Text) )
   {
       return;
   }

   horizontalAxis.clear();
   allVerticalAxis.clear();
   sortedNotNullVerticalAxis.clear();
   chartsName.clear();

   currentlyPickedDataSheet = 0;
   QTextStream in(&file);
   QString readLine;

   while( true )
   {
       readLine = in.readLine();
       if( readLine == NULL )
           return;

       if( readLine.contains("STRT.M") )
           break;
   }

   QString qString = readLine.split(QRegularExpression("[^0-9.-]+"))[2];
   start = qString.toFloat();

   qString = in.readLine().split(QRegularExpression("[^0-9.-]+"))[2];
   stop = qString.toFloat();

   qString = in.readLine().split(QRegularExpression("[^0-9.-]+"))[2];
   step = qString.toFloat();

   qString = in.readLine().split(QRegularExpression("[^0-9.-]+"))[2];
   null = qString.toFloat();

   if( !start || !stop || !step || !null)
       return;

   QStringList headerName;

   while( true )
   {
       readLine = in.readLine();
       if( readLine == NULL )
           return;

       if( readLine.contains("~A") )
       {
           qString = readLine;
           headerName = qString.split(QRegularExpression("\\s+"));
           for( auto&& i : headerName)
           {
               if(i == "~A" || i == "" || i == "DEPTH" || i == "DEPT")
                   continue;
               chartsName.push_back( i );
           }
           break;
       }
    }

   for(size_t i = 0; i < chartsName.size(); i++)
       allVerticalAxis.push_back( std::vector<float>() );

   qString = in.readLine();
   if( qString.contains("#") )
       qString = in.readLine();

   while( true )
   {
       if( qString == NULL )
       {
            if( allVerticalAxis[0].size() == 0 )
               return;
            else
               break;
       }

        QStringList stringList = qString.split(QRegularExpression("[^0-9.-]+"));
        for( int i = 2; i < stringList.size(); i++ )
        {
            if( stringList[i] == "" )
                continue;
            float value = stringList[i].toFloat();
            allVerticalAxis[i - 2].push_back( value );
        }
        qString = in.readLine();
   }

   pickNewDataSheet(0);
   generateHorizontalAxis();
}

void Data::pickNewDataSheet(const int& dataSheet)
{
    currentlyPickedDataSheet = dataSheet;
    generateSortedVerticalData();
}

void Data::generateSortedVerticalData()
{
    sortedNotNullVerticalAxis.clear();
    sortedNotNullVerticalAxis = allVerticalAxis[currentlyPickedDataSheet];
    sortedNotNullVerticalAxis.erase( std::remove( sortedNotNullVerticalAxis.begin(), sortedNotNullVerticalAxis.end(), null ), sortedNotNullVerticalAxis.end() );
    std::sort( sortedNotNullVerticalAxis.begin(), sortedNotNullVerticalAxis.end() );
}

void Data::generateHorizontalAxis()
{
   for(float i = start; i <= stop; i += step)
   {
       horizontalAxis.push_back( i );
   }
}


float Data::getStart() const
{
   return start;
}

float Data::getStop() const
{
   return stop;
}

float Data::getStep() const
{
   return step;
}

float Data::getNull() const
{
   return null;
}

int Data::getCountOfNulls() const
{
   return std::count(allVerticalAxis[currentlyPickedDataSheet].begin(), allVerticalAxis[currentlyPickedDataSheet].end(), null);
}

float Data::getOneValuePointedByPercent( const float& percent ) const
{
    int index = static_cast<int>( (sortedNotNullVerticalAxis.size() - 1) * percent);
    return sortedNotNullVerticalAxis[index];
}

float Data::getOneHorizontalValuePointedByPercent( const float& percent ) const
{
    int index = static_cast<int>( (horizontalAxis.size() - 1) * percent);
    return horizontalAxis[index];
}

bool Data::isAnyNegativeValue() const
{
    return std::count_if(
                sortedNotNullVerticalAxis.begin(),
                sortedNotNullVerticalAxis.end(),
                [](float val) { return val <= 0; }
            ) > 0;
}
