#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QFileDialog>
#include <QPainter>
#include <QDir>
#include <QLabel>
#include <QLineSeries>
#include <QLogValueAxis>
#include <cmath>

using namespace QtCharts;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    chartHeight = 3000;
    isLogarithmic = false;
    isPosOn = false;

    connect( ui->pbFileBtn, SIGNAL(clicked(bool)), this, SLOT(openFilePickWindow()));
    connect( ui->pbPlus, SIGNAL(clicked(bool)), this, SLOT(changeValuePlus()));
    connect( ui->pbMinus, SIGNAL(clicked(bool)), this, SLOT(changeValueMinus()));
    connect( ui->rBPosition, SIGNAL(toggled(bool)), this, SLOT(turnPosition(bool)));

    connect( ui->scaleComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeScale(int)));
    connect( ui->chartsBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeChart(int)));

    ui->chartsBox->hide();
    isChartExist = false;
}

void MainWindow::changeValuePlus()
{
    chartHeight += incrementValue;
    if(chartHeight >= 400)
        ui->pbMinus->setDisabled(false);

    axisY->setTickCount( chartHeight * 0.01 );
    repaint();
}

void MainWindow::changeValueMinus()
{
    if( chartHeight < 400 )
    {
        ui->pbMinus->setDisabled(true);
        return;
    }

    chartHeight -= incrementValue;
    axisY->setTickCount( chartHeight * 0.01 );
    repaint();
}

void MainWindow::turnPosition(bool flag)
{
    isPosOn = flag;
    if( chart )
        chart->setPositionTracing( isPosOn );
}

void MainWindow::openFilePickWindow()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open las file"), QDir::currentPath(), tr("Las Files (*.las *.LAS)"));
    if( fileName == NULL )
        return;

    getData( fileName );
    ui->chartsBox->blockSignals(true);
    ui->chartsBox->clear();

    for( auto& i : data->chartsName )
    {
        ui->chartsBox->addItem( i );
    }

    ui->chartsBox->show();
    ui->chartsBox->blockSignals(false);
}

void MainWindow::changeChart(int toWhich)
{
    data->pickNewDataSheet( toWhich );

    bool isAnyNegative = data->isAnyNegativeValue();

    if(isAnyNegative)
    {
        isLogarithmic = false;
        ui->scaleComboBox->setDisabled(true);
        ui->scaleComboBox->setCurrentIndex( 0 );
    }

    if( !ui->scaleComboBox->isEnabled() && !isAnyNegative )
    {
        ui->scaleComboBox->setDisabled(false);
    }

    generatePoints();
}

void MainWindow::changeScale(int toWhat)
{
    isLogarithmic = toWhat;
    generatePoints();
}

void MainWindow::getData(const QString &fileName)
{
    data = std::make_shared<Data>(fileName);
    generatePoints();
}

void MainWindow::generatePoints()
{
    points.clear();

    for(unsigned int i = 0; i < data->horizontalAxis.size(); i++)
    {
        if( data->allVerticalAxis[data->currentlyPickedDataSheet][i] == data->getNull() )
            continue;

        points.push_back( QPointF{ data->allVerticalAxis[data->currentlyPickedDataSheet][i], data->horizontalAxis[i] } );
    }

    drawChart();
    ui->gridLayoutCanvas->addWidget( chart );
    isChartExist = true;
}

void MainWindow::drawChart()
{
    QLineSeries *series = new QLineSeries();

    if( data )
    {
        for(auto& i : points)
        {
            *series << i;
        }

    }
    if( isChartExist ){
        isChartExist = false;
        delete this->chart;
    }

   QChart* chartModel = new QChart();

    if( isLogarithmic )
    {
        chartModel->legend()->hide();
        chartModel->addSeries(series);

        QLogValueAxis *axisX = new QLogValueAxis();
        axisX->setLabelFormat("%g");
        axisX->setBase(10.0);
        axisX->setRange(0.1, 100000);
        axisX->setMinorTickCount(-1);
        chartModel->addAxis(axisX, Qt::AlignTop);
        series->attachAxis(axisX);
    }
    else
    {
        chartModel->legend()->hide();
        chartModel->addSeries(series);

        QValueAxis *axisX = new QValueAxis();
        axisX->setLabelFormat("%i");
        axisX->setTickCount( 5 );
        chartModel->addAxis(axisX, Qt::AlignTop);
        series->attachAxis(axisX);
    }

    axisY = new QValueAxis();
    axisY->setReverse(true);
    axisY->setLabelFormat("%i");
    axisY->setTickCount( 30 );
    chartModel->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);

    chartModel->setTheme(  QChart::ChartThemeDark );

    this->chart = new ChartView(chartModel, isLogarithmic, isPosOn);
    this->chart->setRenderHint(QPainter::Antialiasing);

}

void MainWindow::paintEvent(QPaintEvent *event)
{
    if( chart )
    {
        this->chart->resize(this->width() - 173, chartHeight);
        ui->scrollAreaWidgetContents->resize(this->width() - 173, chartHeight);
    }

}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch( event->key() )
    {
        case Qt::Key_Plus:
            changeValuePlus();
            break;
        case Qt::Key_Minus:
            changeValueMinus();
            break;
        default:
            QWidget::keyPressEvent(event);
            break;
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
