#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QComboBox>
#include "data.h"
#include <memory>
#include <QChartView>
#include "chartview.h"
#include <QValueAxis>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
   void openFilePickWindow();
   void changeScale(int);
   void changeChart(int);
   void changeValueMinus();
   void changeValuePlus();
   void turnPosition(bool);

protected:
   void paintEvent(QPaintEvent *) override;
   void keyPressEvent(QKeyEvent *) override;

private:
    Ui::MainWindow *ui;
    QComboBox* comboBox;
    ChartView * chart = nullptr;
    QtCharts::QValueAxis *axisY = nullptr;

    bool isChartExist;
    bool isLogarithmic;
    bool isPosOn;

    void drawChart();
    std::shared_ptr<Data> data = nullptr;
    std::vector<QPointF> points;

    const int incrementValue = 100;
    int chartHeight;

    void getData( const QString& fileName );
    void generatePoints();
};

#endif // MAINWINDOW_H
